FROM maven
EXPOSE 8080

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
 
COPY src ./src

RUN mvn clean install
ADD *.jar  uni-league.jar
ENTRYPOINT ["java","-jar", "uni-league.jar"]
